import { ComponentInteraction, Message } from 'oceanic.js';

export interface Command {
	names: string[];
	description: string;
	usage: string;
	secret?: boolean;
	canUse: (message: Message) => boolean;
	run: (message: Message, args: string[]) => Promise<void>;
	ready?: () => void;
}

export interface InteractionHandler {
	handle: (interaction: ComponentInteraction) => void;
	[others: string]: any;
}

export interface Replyable {
	id: string;
	channelID: string;
	[others: string]: any;
}

export type VoteTally = Map<string, Set<string>>;
export type VoteTallyObj = {[answer: string]: string[]};

export type fsPoll = {
	guildId: string;
	channelId: string;
	messageId: string;
	end: number;
	tally: VoteTallyObj;
};

export type GitInfo = {
	repo: string | null;
	revision: string | null;
}

// Samu's API stuff

export type VoteAPIFetch = {
	message: Message;
	method: 'GET' | 'POST';
	token?: string;
	[data: string]: any;
};

export type VoteAPIResponse<T> = T | VoteAPIError;

export type VoteAPIError = {
	error: string;
};

export type VoteAPIRegister = {
	discordId: string;
	invite: string;
	token: string; // added in 2.1.8
	iEventId: string;
};

export type VoteAPILogin = {
	token: string;
	discordId: string;
	discordSlug: string;
	discordPfpUrl: string;
	admin: boolean;
};

export type VoteAPISuggest = {
	discordId: string;
	// below are populated if the IE was created
	eventId?: string;
	createdAt?: Date;
	endsAt?: Date;
	duration?: number;
	ended?: boolean;
};

export type VoteAPIEvent = {
	eventId: string;
	discordId: string;
	discordUser: string;
	discordSlug: string;
	discordPfpUrl: string;
	ended: boolean;
	createdAt: Date;
	endsAt: Date;
	duration: number;
	positiveVotesInt: number;
	negativeVotesInt: number;
};

export type VoteAPIGetall = {
	events: VoteAPIEvent[];
};

export type VoteAPIVote = {
	iEventId: string;
	createdAt: Date;
};

export type VoteAPIRole = {
	roleColor?: string;
	roleName?: string;
	roleId?: string;
	discordId?: string;
	createdAt?: Date;
	editedAt?: Date;
};
