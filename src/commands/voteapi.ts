import { Message } from 'oceanic.js';
import { persistentStore, prefix, reply } from '..';
import { Command, VoteAPIError, VoteAPIFetch, VoteAPIGetall, VoteAPILogin, VoteAPIRegister, VoteAPIResponse, VoteAPIRole, VoteAPISuggest, VoteAPIVote } from '../typedef';

// https://github.com/unfinishedprojs/demo-server/blob/main/docs/v2.md

const API_URL = 'https://api.samu.lol/api/v2';

async function fetchAPI<T>(endpoint: string, settings: VoteAPIFetch): Promise<T | null> {
	const data: {[key: string]: any} = {};
	for(const [k, v] of Object.entries(settings))
		if(!['message', 'token'].includes(k))
			data[k] = v;

	try {
		let body: string | undefined, url: string = `${API_URL}/${endpoint}`;
		if(settings.method == 'GET') {
			let q: string = '?';
			for(const [k, v] of Object.entries(data))
				q += `${encodeURIComponent(k)}=${encodeURIComponent(v)}&`;
			url += q.slice(0, -1);
			body = undefined;
		} else
			body = JSON.stringify(data);

		const resp: Response = await fetch(url, {
			headers: {
				'Content-Type': 'application/json',
				Authorization: settings.token ?? persistentStore.get(voteapi, settings.message.author.id)?.token ?? 'no token',
				'User-Agent': 'qtbot'
			},
			method: settings.method,
			body
		});
		const json: VoteAPIResponse<T> = await resp.json();
		if(!resp.ok) {
			// reply(settings.message, {content: `${resp.status} ${resp.statusText} \`\`\`json\n${JSON.stringify(json as VoteAPIError, null, '\t')}\n\`\`\``});
			reply(settings.message, {content: (json as VoteAPIError).error ?? 'samu is fucking stupid and left the error field on a non-200 response empty'});
			return null;
		}
		return json as T;
	} catch(e) {
		reply(settings.message, {content: 'something went extremely wrong, sowwy.'});
		return null;
	}
}

const voteapi: Command = {
	names: ['voteapi', 'vote', 'v'],
	usage: '`ð register <invite> <...password>`\n`ð login <...password>`\n\n`ð role <name... | -keep> <hex color>` to update or create a custom color role\n`ð suggest <mention/userId>` to suggest a user to vote on\n`ð` to list all currently ongoing voting events\n`ð ended` to list all ended vote events\n`ð -<eventId> <yes/no / positive/negative>` to vote',
	description: 'samu\'s v2 API client implementation',
	canUse: () => true,
	secret: true,
	async run(message: Message, _args: string[]) {
		let store = persistentStore.get(this, message.author.id);
		if(typeof store != 'object') {
			store = {
				token: typeof store == 'string'? store : null,
				voted: []
			};
			persistentStore.put(this, message.author.id, store);
		}

		const [name, ...args]: string[] = _args;
		switch(name) {
			case 'register': {
				if(message.guildID) {
					reply(message, {content: 'Please go to bot dms and don\'t use the same password ;-;'});
					return;
				}
				const [invite, ...pwd] = args;
				const password: string = pwd.join(' ');
				if(!invite) {
					reply(message, {content: 'No invite specified'});
					return;
				}
				if(!password) {
					reply(message, {content: 'No password specified'});
					return;
				}
				const json: VoteAPIRegister | null = await fetchAPI('users/register', {
					method: 'POST',
					message,
					invite,
					password,
					discordId: message.author.id
				});
				if(!json)
					return;
				if(json.token) {
					store.token = json.token;
					persistentStore._save();
					reply(message, {content: 'Registration and login successful, go vote!'});
				} else
					reply(message, {content: `Registration successful, now \`${prefix}voteapi login\` using the same password`});
				break;
			}
			case 'login': {
				const password: string | undefined = args.join(' ');
				if(!password) {
					reply(message, {content: 'No password specified'});
					return;
				}
				const json: VoteAPILogin | null = await fetchAPI('users/login', {
					method: 'POST',
					message,
					password,
					discordId: message.author.id
				});
				if(!json)
					return;
				reply(message, {content: 'Login successful'});
				store.token = json.token;
				persistentStore._save();
				break;
			}
			case 'suggest': {
				const user: string | undefined = args[0];
				if(!user) {
					reply(message, {content: 'No user specified'});
					return;
				}
				const uid = user.match(/(\d+)/);
				if(!uid) {
					reply(message, {content: 'User has to be either a user id or a @mention'});
					return;
				}
				const json: VoteAPISuggest | null = await fetchAPI('ievents/suggest', {
					method: 'POST',
					message,
					discordId: uid[0]
				});
				if(!json)
					return;

				if(json.eventId)
					reply(message, {content: `You can now vote on ${user}! \`tv -${json.eventId}\` y/n`});
				else
					reply(message, {content: `You'll have to wait until more users suggest ${user}`});
				break;
			}
			case 'role': {
				let name: string = args.slice(0, -1).join(' ').trim();
				let color: string | undefined = args.at(-1)?.trim();

				if(!name || !color) {
					reply(message, {content: 'No name or color specified'});
					return;
				}

				if(name == '-keep') {
					const json: VoteAPIRole | null = await fetchAPI('users/role', {
						method: 'GET',
						message
					});
					if(!json)
						return;
					if(!json.roleName) {
						reply(message, {content: 'Specified keep name but no previous role was found'});
						return;
					}
					name = json.roleName;
				}

				if(color.startsWith('#'))
					color = color.slice(1);

				if(!color || !color.match(/[0-f]{6}/)) {
					reply(message, {content: 'Invalid color specified (must be hex code like #abcdef or 423152)'});
					return;
				}

				const json: VoteAPIRole | null = await fetchAPI('users/role', {
					method: 'POST',
					message,
					roleName: name,
					roleColour: color
				});
				if(!json)
					return;
				reply(message, {content: 'Role updated'});
				break;
			}
			default: {
				if(!name || name == 'ended') {
					const ended: boolean = name == 'ended';
					const json: VoteAPIGetall | null = await fetchAPI('ievents/getall', {
						method: 'GET',
						message,
						ended
					});
					if(!json)
						return;
					if(json.events.length == 0) {
						reply(message, {content: ended? 'No ended vote events yet' : 'Not voting for anyone right now!'});
						return;
					}
					let resp: string = ended? 'Ended votes:\n' : 'Currently voting for:\n';
					let replied: boolean = false;
					for(const event of json.events.toSorted((a, b) => +new Date(b.endsAt) - +new Date(a.endsAt))) {
						const votes: number = event.positiveVotesInt + event.negativeVotesInt;
						let line: string = `<@${event.discordId}>, ${ended? 'ended' : 'ends'} <t:${Math.round(+(new Date(event.endsAt)) / 1000)}:R> - `;
						// this technically could be done in ``, but that would get messy
						if(votes != 0)
							line += `${Math.round(event.positiveVotesInt / votes * 100)}% positive (${event.positiveVotesInt} yay to ${event.negativeVotesInt} nay votes)`;
						else
							if(ended)
								line += 'no one voted';
							else
								line += 'no one voted yet!';
						if(!ended && !store.voted.includes(event.eventId))
							line += `, vote with \`${prefix}v -${event.eventId}\``;
						line += '\n';

						if(resp.length + line.length > 2000) {
							reply(message, {content: resp});
							replied = true;
							resp = '';
						}
						resp += line;
					}
					if(resp.length > 0)
						reply(message, replied? {content: resp, messageReference: {}} : {content: resp});
					break;
				}

				if(name[0] != '-') {
					reply(message, {content: `See ${prefix}help voteapi`});
					return;
				}
				const option: string | undefined = args[0];
				if(!option || (option[0] != 'y' && option[0] != 'p' && option[0] != 'n')) {
					reply(message, {content: 'Vote yay or nay? positive or negative?'});
					return;
				}
				const endpoint: string = option[0] == 'y' || option[0] == 'p'? 'positive' : 'negative';
				const json: VoteAPIVote | null = await fetchAPI(`ievents/vote/${endpoint}`, {
					method: 'POST',
					message,
					eventId: name.slice(1)
				});
				if(!json)
					return;
				reply(message, {content: 'Voted!'});
				store.voted.push(name.slice(1));
				persistentStore._save();
				break;
			}
		}
	}
};

export default voteapi;
