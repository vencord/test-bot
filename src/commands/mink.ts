import { Message } from 'oceanic.js';
import { reply } from '..';
import { Command } from '../typedef';

const mink: Command = {
	names: ['mink', 'minky', 'minker'],
	usage: '`ð`',
	description: 'MINKY',
	canUse: () => true,
	async run(message: Message) {
		const resp = await fetch('https://rozbrajacz.futbol/api', {
			redirect: 'manual',
			headers: {
				'User-Agent': 'qtbot'
			}
		});

		reply(message, {content: resp.status != 303? 'no mink :(' : 'https://rozbrajacz.futbol' + resp.headers.get('Location')});
	}
};

export default mink;
