import { AnyTextableGuildChannel, ButtonStyles, ComponentInteraction, ComponentTypes, Guild, Message, MessageActionRow, MessageFlags, TextButton } from 'oceanic.js';
import { client, interactions, persistentStore, prefix, reply } from '..';
import { Command, InteractionHandler, VoteTally, VoteTallyObj, fsPoll } from '../typedef';

function tallyToObject(tally: VoteTally): VoteTallyObj {
	const ret: VoteTallyObj = {};
	for(const [k, v] of tally)
		ret[k] = [...v];
	return ret;
}

function objectToTally(object: VoteTallyObj): VoteTally {
	const ret: VoteTally = new Map;
	for(const [k, v] of Object.entries(object))
		ret.set(k, new Set(v));
	return ret;
}

function getStore(): fsPoll[] {
	return persistentStore.get(poll, 'polls') as fsPoll[];
}

function defaultPollHandle(options: string[], message: Message, time: number, save: boolean = true): InteractionHandler {
	const tally: VoteTally = new Map;
	for(const option of options)
		tally.set(option, new Set);

	if(save) {
		getStore().push({
			guildId: message.guildID!,
			channelId: message.channelID,
			messageId: message.id,
			end: Date.now() + time,
			tally: tallyToObject(tally)
		});
		persistentStore._save();
	}

	setTimeout(defaultPollTimeout(message), Math.max(time, 1));

	return {
		tally,
		handle(interaction: ComponentInteraction) {
			const uid: string = interaction.user.id;
			let changedVote: boolean = false;

			for(const key of options)
				changedVote = tally.get(key)!.delete(uid) || changedVote;

			tally.get(interaction.data.customID)!.add(uid);

			const pollIdx: number = getStore().findIndex(poll => poll.messageId == message.id)!;
			getStore()[pollIdx].tally = tallyToObject(tally);
			persistentStore._save();

			interaction.createMessage({
				content: `${changedVote? 'Now voting' : 'Voted'} for ${interaction.data.customID}`,
				flags: MessageFlags.EPHEMERAL
			});
		}
	};
}

function defaultPollTimeout(message: Message): () => void {
	return () => {
		const handler: InteractionHandler | undefined = interactions.get(message.id);
		if(!handler)
			return;

		const tally: VoteTally = handler.tally;
		interactions.delete(message.id);
		persistentStore.put(poll, 'polls', getStore().filter(poll => poll.messageId != message.id));

		let results: string = '';
		const sum: number = Array.from(tally.values()).map(ids => ids.size).reduce((prev, curr) => prev + curr);

		if(sum == 0)
			results += 'no one voted, sad.';
		else
			Array.from(tally.keys()).toSorted((a: string, b: string) => tally.get(b)!.size - tally.get(a)!.size).forEach((name: string) => {
				results += `${name} - ${(tally.get(name)!.size / sum * 100).toFixed(2)}%\n`;
			});

		message.edit({
			content: message.content.slice(0, message.content.lastIndexOf('\n')) + '\npoll ended'
		});

		reply(message, {
			content: `### Poll ended\n${results.trimEnd()}\n(${sum} ${sum == 1? 'person' : 'people'} voted)`
		});
	};
}

const poll: Command = {
	names: ['poll', 'p'],
	usage: '`ð <time> <message>` for a simple yes/no poll\n`ð <time> <message> ~ <option>, <option> [, option... max 5]`\n`ð -end` while replying to a poll to end it',
	description: 'create a poll',
	canUse: (message: Message) => Boolean(message.guildID),
	async run(message: Message, args: string[]) {
		const row: MessageActionRow = {
			type: ComponentTypes.ACTION_ROW,
			components: [
				{
					customID: 'yes',
					label: 'yes',
					style: ButtonStyles.SUCCESS,
					type: ComponentTypes.BUTTON
				} as TextButton,
				{
					customID: 'no',
					label: 'no',
					style: ButtonStyles.DANGER,
					type: ComponentTypes.BUTTON
				} as TextButton
			] as TextButton[]
		};

		if(args.length == 1 && args[0] == '-end') {
			if(!message.messageReference || !interactions.get(message.messageReference!.messageID!)?.tally) {
				reply(message, {content: 'you have to reply to the poll'});
				return;
			}
			if(message.referencedMessage!.referencedMessage!.author.id == message.author.id || message.member!.permissions.has('MANAGE_MESSAGES'))
				defaultPollTimeout(message.referencedMessage!)(); // this technically leaves a setTimeout strangled shrug
			else
				reply(message, {content: '¯\\\\_(ツ)\\_/¯ (you\'re not the poll author nor do you have the manage messages permission)'});
			return;
		}

		const [ptime, ...other]: string[] = args;
		const [text, options]: string[] = other.join(' ').split(' ~ ', 2);

		if(!ptime || ptime.length == 0) {
			reply(message, { content: `you have to provide the length of time the poll runs for (see ${prefix}help poll)` });
			return;
		}

		const [pamount, unit]: string[] = [ptime.slice(0, -1), ptime.at(-1)!];
		const amount: number = parseInt(pamount);
		const units: Map<string, number> = new Map([
			['s', 1000],
			['m', 1000 * 60],
			['h', 1000 * 60 * 60],
			['d', 1000 * 60 * 60 * 24],
			['w', 1000 * 60 * 60 * 24 * 7]
		]);

		if(amount <= 0 || !units.has(unit)) {
			reply(message, { content: 'invalid time period, needs to be: `<amount><s/m/h/d/w>`' });
			return;
		}

		const time: number = amount * units.get(unit)!;

		if(time >= 2 ** 31 - 1) {
			reply(message, { content: 'we ain\'t waiting that long' });
			return;
		}

		if(text.length == 0) {
			reply(message, { content: 'you have to provide what you want to poll about' });
			return;
		}

		if(text.length >= 1000) {
			reply(message, { content: 'cmon that\'s too long <:nya:1245857523429408779>' });
			return;
		}

		if(options && options.length > 0) {
			const buttons: string[] = Array.from(new Set(options.split(', ').filter(name => name.length > 0))); // deduplicate
			if(buttons.length < 2) {
				reply(message, { content: 'there must be at least 2 custom options' });
				return;
			}
			// API limitations, https://discord.com/developers/docs/interactions/message-components
			if(buttons.length > 5) { // TODO? - if we split into multiple ActionRows, we can get up to 25 buttons!
				reply(message, { content: 'there must be no more than 5 custom options' });
				return;
			}
			if(buttons.filter(name => name.length > 80).length > 0) {
				reply(message, { content: 'options have to be 80 characters maximum' });
				return;
			}
			row.components = buttons.map((name: string, i: number) => ({
				customID: name,
				label: name,
				style: i == 0? ButtonStyles.SUCCESS : i == buttons.length - 1? ButtonStyles.DANGER : i % 2 == 0? ButtonStyles.SECONDARY : ButtonStyles.PRIMARY,
				type: ComponentTypes.BUTTON
			} as TextButton)) as TextButton[];
		}

		const newMessage: Message = await reply(message, {
			content: text + `\npoll ends <t:${Math.round((Date.now() + time) / 1000)}:R>`,
			components: [row]
		});

		interactions.set(newMessage.id, defaultPollHandle(row.components.map(button => (button as TextButton).customID), newMessage, time));
	},
	async ready() {
		const polls: fsPoll[] = getStore();

		// no polls to restore = no problem to worry about
		if(!polls || polls.length == 0) {
			persistentStore.put(this, 'polls', []);
			return;
		}

		const restoredPolls: fsPoll[] = [];

		console.log(`Restoring ${polls.length} poll${polls.length == 1? '' : 's'}`);
		for(const poll of polls) {
			let message: Message;
			try {
				const guild: Guild | undefined = client.guilds.get(poll.guildId);
				if(!guild) // guild deleted / kicked from
					continue;

				const channel: AnyTextableGuildChannel | undefined = guild.channels.get(poll.channelId) as AnyTextableGuildChannel;
				if(!channel) // channel deleted
					continue;

				message = await channel.getMessage(poll.messageId);
				if(!message) // message deleted
					continue;
			} catch {
				continue;
			}
			const tally: VoteTally = objectToTally(poll.tally);
			interactions.set(message.id, defaultPollHandle([], message, poll.end - Date.now(), false));
			interactions.get(message.id)!.tally = tally;

			restoredPolls.push(poll);
		}
		console.log(`Restored ${restoredPolls.length} poll${restoredPolls.length == 1? '' : 's'}`);
		persistentStore.put(this, 'polls', restoredPolls);
	}
};

export default poll;
