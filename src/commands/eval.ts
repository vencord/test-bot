import { inspect } from 'node:util';
import { Message } from 'oceanic.js';
import { reply } from '..';
import { Command } from '../typedef';

const _eval: Command = {
	names: ['eval', 'e'],
	usage: '`ð <code>`',
	description: 'evaluate an expression',
	canUse: () => true,
	async run(message: Message, args: string[]) {
		let temp: any, result: string;
		const toEval: string = args.join(' ').replaceAll('ð', 'require("..").');

		try {
			temp = eval(toEval);
			if(temp instanceof Promise)
				temp = await temp;
			result = inspect(temp);
		} catch(e: any) {
			result = inspect(e);
		}

		if(result.length > 1980) {
			result = result.slice(0, 1980) + '…';
		}

		reply(message, {content: result == ''? '<no content>' : `\`\`\`js\n${result}\n\`\`\``});
	}
};

export default _eval;
