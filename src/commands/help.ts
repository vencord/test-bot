import { Message } from 'oceanic.js';
import { client, commands, gitInfo, prefix, reply } from '../index';
import { Command } from '../typedef';

const help: Command = {
	names: ['help', 'h'],
	usage: '`ð [cmd]`',
	description: 'shows help… who could\'ve guessed.',
	canUse: () => true,
	async run(message: Message, args: string[]) {
		if(args.length > 0 && commands.has(args[0]) && commands.get(args[0])!.canUse(message)) {
			const command: Command = commands.get(args[0])!;
			reply(message, {content: `### ${command.names.map(name => prefix + name).join(', ')}\n${command.usage.replaceAll('ð', prefix + command.names[0])}\n${command.description}`});
			return;
		}

		let name: string = client.user.username;
		if(gitInfo.repo && gitInfo.revision)
			name = `[${name}](<${gitInfo.repo}/commit/${gitInfo.revision.slice(0, 8)}>)`;

		let ret: string = `${name} commands:\n`;

		const uniqueCommands: Set<Command> = new Set(commands.values());
		let maxlen: number = 0;

		const filteredCommands: Command[] = [];
		for(const cmd of uniqueCommands) {
			if(!cmd.canUse(message))
				continue;

			filteredCommands.push(cmd);
			maxlen = Math.max(maxlen, cmd.names[0].length);
		}

		for(const cmd of filteredCommands)
			ret += `\`${prefix}${cmd.names[0]}${' '.repeat(maxlen - cmd.names[0].length)}\` ${cmd.description}\n`;

		reply(message, {content: ret.trimEnd()});
	}
};

export default help;
