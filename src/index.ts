import { readFile, readdir, writeFile } from 'node:fs/promises';
import { ActivityTypes, Client, ComponentInteraction, Constants, CreateMessageOptions, Message, PossiblyUncachedMessage } from 'oceanic.js';
import config from './config';
import { Command, GitInfo, InteractionHandler, Replyable } from './typedef';

if(!config.token) {
	console.error('no token :3');
	process.exit(1);
}

const client: Client = new Client({
	auth: `Bot ${config.token}`,
	gateway: {
		intents: Constants.AllNonPrivilegedIntents | Constants.Intents.MESSAGE_CONTENT
	}
});

const persistentStore = {
	_store: {} as {[command: string]: {[key: string]: any}},

	async _save(): Promise<void> {
		await writeFile('persistentStore.json', JSON.stringify(this._store, null, '\t'));
	},
	async _load(): Promise<void> {
		try {
			this._store = JSON.parse((await readFile('persistentStore.json')).toString());
		} catch {} // file doesn't exist yet on fs
	},

	put(command: Command, key: string, value: any): void {
		const name: string = command.names[0];
		if(!this._store[name])
			this._store[name] = {};
		this._store[name][key] = value;
		this._save();
	},
	get(command: Command, key: string): any {
		return this._store[command.names[0]]?.[key];
	}
};

const prefix: string = 't';
const commands: Map<string, Command> = new Map;
const interactions: Map<string, InteractionHandler> = new Map;
const gitInfo: GitInfo = {repo: null, revision: null};

client.once('ready', async () => {
	console.log(`Logged in as ${client.user.tag}`);
	console.log(`Currently in ${client.guilds.size} guild${client.guilds.size == 1? '' : 's'}`);

	// set status to 'on hostname' or 'with you' if no /etc/hostname (like windows, ew.)
	let playing: string = 'with you';
	try {
		playing = `on ${await readFile('/etc/hostname')}`.trim();
	} catch {}
	client.editStatus('online', [{
		type: ActivityTypes.GAME,
		name: playing
	}]);
	console.log(`Status set to 'Playing ${playing}'`);

	// find git repo info
	try {
		const gitConfig: string = (await readFile('.git/config')).toString();
		const remote: string | undefined = gitConfig.split('\n[').find(section => section.startsWith('remote'));
		if(remote) {
			const urlLine: string | undefined = remote.split('\n').map(line => line.trim()).find(line => line.startsWith('url'));
			if(urlLine) {
				let url: string = urlLine.slice(urlLine.indexOf('=') + 1).trim();
				if(url.endsWith('.git'))
					url = url.slice(0, -4);
				if(url.startsWith('git@'))
					url = 'https://' + url.slice(4).replace(':', '/');
				gitInfo.repo = url;
			}
		}

		const gitHEAD: string = (await readFile('.git/HEAD')).toString();
		if(gitHEAD.startsWith('ref:'))
			gitInfo.revision = (await readFile('.git/' + gitHEAD.slice(4).trim())).toString().trim();
		else
			gitInfo.revision = gitHEAD.trim();
	} catch {} // total fail is null/null, better case is partial info

	// load persistentStore from fs
	await persistentStore._load();

	// command registration
	for(const fn of await readdir('src/commands')) {
		if(!fn.endsWith('.ts'))
			continue;

		const cmd: Command = (await import(`./commands/${fn.slice(0, -3)}`)).default;
		for(const name of cmd.names)
			commands.set(name, cmd);

		if(cmd.ready)
			cmd.ready();
	}
});

client.on('error', err => {
	console.error('-/ client.on(error) \\-');
	console.error(err);
	console.error('-\\ client.on(error) /-');
});

client.on('messageCreate', async message => {
	if(message.author.bot || !message.content.startsWith(prefix))
		return;

	const [_cmd, ...args]: string[] = message.content.slice(prefix.length).split(' ');
	const cmd: string = _cmd.toLowerCase();
	if(commands.has(cmd)) {
		const command: Command = commands.get(cmd)!;
		const run: boolean = command.canUse(message);
		const logMessage = `${message.author.username} ${run? 'ran' : 'tried to run'} '${message.content.replaceAll('\n', '\\n')}' (${command.names[0]}) in ${message.guild?.name ?? message.guildID ?? 'DMs'}`;
		if(run) {
			await command.run(message, args);
			console.log(command.secret? `${message.author.username} ran ${command.names[0]} (secret) in ${message.guild?.name ?? message.guildID ?? 'DMs'}` : logMessage);
		} else
			console.log(logMessage);

	}
});

client.on('interactionCreate', interaction => {
	if(!(interaction instanceof ComponentInteraction)) {
		console.log(`${interaction.user.username} interacted with ${interaction.id}`);
		reply(interaction, { content: 'how did you end up here? (interactionCreate: interaction **not** instanceof ComponentInteraction)' });
		return;
	}

	console.log(`${interaction.user.username} interacted with '${interaction.data.customID}' on ${interaction.id} (message: '${interaction.message.content.replaceAll('\n', '\\n')}', id: ${interaction.message.id})`);

	if(!interactions.has(interaction.message.id)) {
		interaction.createMessage({
			content: 'that interaction does not seem to be handled by anything, sorry.',
			flags: Constants.MessageFlags.EPHEMERAL
		});
		return;
	}

	interactions.get(interaction.message.id)!.handle(interaction);
});

client.on('messageDelete', (message: PossiblyUncachedMessage) => {
	if(interactions.has(message.id))
		interactions.delete(message.id);
});

client.on('messageDeleteBulk', (messages: PossiblyUncachedMessage[]) => {
	for(const message of messages)
		if(interactions.has(message.id))
			interactions.delete(message.id);
});

function reply(to: Replyable, message: CreateMessageOptions): Promise<Message> {
	// allow passing {} to remove the default
	if(message.messageReference && !message.messageReference.messageID)
		delete message.messageReference;
	else
		message.messageReference = message.messageReference ?? {messageID: to.id};
	message.allowedMentions = message.allowedMentions ?? {
		repliedUser: true,
		everyone: false,
		roles: false,
		users: false
	};
	return client.rest.channels.createMessage(to.channelID, message);
}

export { client, commands, gitInfo, interactions, persistentStore, prefix, reply };

client.connect();
