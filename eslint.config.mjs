// @ts-check

import tseslint from 'typescript-eslint';

export default tseslint.config(
	...tseslint.configs.recommended,
	{
		rules: {
			'@typescript-eslint/no-explicit-any': 'off', // this is such a stupid rule
			indent: ['error', 'tab', {SwitchCase: 1}],
			'linebreak-style': ['error', 'unix'],
			quotes: ['error', 'single', {allowTemplateLiterals: true}],
			semi: ['error', 'always'],
			'space-before-function-paren': ['error', {anonymous: 'always', named: 'never', asyncArrow: 'always'}],
			'space-in-parens': ['error', 'never'],
			'prefer-exponentiation-operator': ['error'],
			'no-unneeded-ternary': ['error'],
			'multiline-ternary': ['error', 'never'],
			'eol-last': ['error', 'always'],
			'dot-location': ['error', 'property'],
			'space-before-blocks': ['error', 'always'],
			'array-bracket-spacing': ['error', 'never'],
			'arrow-spacing': ['error', {before: true, after: true}],
			'comma-dangle': ['error', 'never'],
			'brace-style': ['error', '1tbs', {allowSingleLine: true}],
			'func-call-spacing': ['error', 'never']
		}
	}
).map(ruleset => {
	ruleset.files = ['src/**/*.ts', 'src/*.ts'];
	return ruleset;
});
